# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from common.logger import Logger
from base.base import BasePage
from selenium.webdriver.support import expected_conditions as EC

class CommodityListAssert:

    def __init__(self, driver):
        self.driver = driver

    # 断言
    # def enter_search_assert(self):
    #     rr = self.driver.find_element_by_xpath(
    #         '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]').text
    #     print("hhhhhhhhhh",rr)
    #
    #     assert rr == "华为 HUAWEI P20"
    # """商品名称搜索断言"""
    # def enter_search_assert(self):
    #     try:
    #         rr = self.driver.find_element_by_xpath(
    #             '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]').text
    #         print("hhhhhhhhhh", rr)
    #
    #         assert rr == "华为 HUAWEI P20"
    #         return True
    #     except:
    #         Logger.info("断言失败，用例执行失败")
    #         return False
    # """货号搜索断言"""
    # def product_code_assert(self):
    #     try:
    #         rr = self.driver.find_element_by_xpath(
    #             '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr[1]/td[5]/div/p[2]').text
    #         assert rr == "货号：6946605"
    #         return True
    #     except:
    #         Logger.info("断言失败，用例执行失败")
    #         return False
    #
    # def data_storage_assert(self):
    #     try:
    #         WebDriverWait(self.driver,15).\
    #             until(EC.visibility_of_element_located((By.XPATH,'//*[@id="mainScreen"]/div/div/div/div[1]/div[1]/div')))
    #         Logger.info("进入数据存储页面成功，用例执行成功")
    #         return True
    #     except:
    #         Logger.info("断言失败，用例执行失败")
    #         return False

    """商品名称搜索断言"""
    def enter_search_assert(self, text):
        return BasePage(self.driver).assert_text(
            (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]'), text, doc="商品名称搜索断言" + text)

    """商品名称搜索断言"""
    def commodity_brand_assert(self, text):
        return BasePage(self.driver).assert_text(
            (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]'), text, doc="商品品牌搜索断言" + text)

    """货号搜索断言"""
    def product_code_assert(self, text):
        return BasePage(self.driver).assert_text(
            (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr[1]/td[5]/div/p[2]'), text, doc="货号搜索断言" + text)

    """上下架搜索断言"""
    def down_shelf_assert(self, text):
        return BasePage(self.driver).assert_text(
            (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]'), text, doc="上下架搜索断言" + text)
    #
    # def down_shelf_assert(self):
    #     try:
    #         WebDriverWait(self.driver,15).\
    #             until(EC.visibility_of_element_located((By.XPATH,'//div[class="el-switch is-checked" and aria-checked="true"]')))
    #         Logger.info("进入数据存储页面成功，用例执行成功")
    #         return True
    #     except:
    #         Logger.info("断言失败，用例执行失败")
    #         return False
    # def data_center_assert(self, text):
    #     return BasePage(self.driver).assert_text(
    #         (By.XPATH, '//div[@class="notify-content"]'), text, doc="提示" + text)

