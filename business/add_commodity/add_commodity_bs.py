# coding=utf-8
from base.base import *
from base.base import BasePage
from pagelocators.commodity_locs.add_commodity_page import AddCommodityPage as ACP
import pyautogui
class AddCommodityBusiness(BasePage):

    """正常添加商品功能"""
    def add_commodity(self):
        time.sleep(2)
        self.click_element(ACP.commodity_loc,doc='点击商品')
        self.click_element(ACP.commodity_list_loc,doc='点击添加商品列表')
        self.click_element(ACP.commodity_class_loc,doc='点击商品分类')
        self.click_element(ACP.one_clothing_class_loc,doc='点击一级商品分类-服装')
        self.click_element(ACP.two_Tshirt_class_loc,doc='点击二级商品分类-T恤')
        self.input_text(ACP.commodity_name_loc,text='道长牌T恤',doc='输入道长牌T恤')
        self.input_text(ACP.subtitle_loc,text='道长牌T恤-慧测',doc='输入副标题-道长牌T恤-慧测')
        self.click_element(ACP.commodity_brand_loc,doc='点击品牌')
        time.sleep(2)
        self.click_element(ACP.Apple_brand_loc,doc='点击苹果品牌')

        self.click_element(ACP.next_promotion_loc,doc='点击下一步填写商品促销')
        self.click_element(ACP.next_properties_loc,doc='点击下一步填写商品属性')
        self.click_element(ACP.next_association_loc,doc='点击下一步选择商品关联')
        self.click_element(ACP.submit_complete_loc,doc='点击完成提交商品')
        pyautogui.press('enter')
        # self.click_element(ACP.submit_yes_loc,doc='点击完成提交商品')



    # """商品名称搜索功能"""
    # def product_code(self):
    #     self.click_element(ACP.commodity_loc,doc='点击商品')
    #     self.click_element(ACP.commodity_list_loc,doc='点击商品列表')
    #     self.click_element(ACP.reset_loc,doc='点击重置')
    #     self.click_element(ACP.product_code_loc,doc='点击货号')
    #     self.input_text(ACP.product_code_loc,text='6946605',doc='输入货号：6946605')
    #     self.click_element(ACP.query_results_loc,doc='点击查询结果')
    #     time.sleep(3)
    #     self.get_text(ACP.enter_searchassert_loc,doc='输入搜索断言元素')
    #
    # """商品品牌搜索功能"""
    # def commodity_brand(self):
    #     self.click_element(ACP.commodity_loc,doc='点击商品')
    #     self.click_element(ACP.commodity_list_loc,doc='点击商品列表')
    #     self.click_element(ACP.reset_loc,doc='点击重置')
    #     self.click_element(ACP.commodity_brand_loc,doc='点击品牌')
    #     time.sleep(2)
    #     self.click_element(ACP.Apple_brand_loc,doc='点击苹果品牌')
    #     self.click_element(ACP.query_results_loc,doc='点击查询结果')
    #     time.sleep(3)
    #     self.get_text(ACP.enter_searchassert_loc,doc='输入搜索断言元素')
    #
    # """商品上架状态搜索功能"""
    # def shelf_status(self):
    #     self.click_element(ACP.commodity_loc,doc='点击商品')
    #     self.click_element(ACP.commodity_list_loc,doc='点击商品列表')
    #     self.click_element(ACP.reset_loc,doc='点击重置')
    #     self.click_element(ACP.shelf_status_loc,doc='点击上下架按钮状态')
    #     time.sleep(2)
    #     self.click_element(ACP.down_shelf_loc,doc='点击下架按钮')
    #     self.click_element(ACP.query_results_loc,doc='点击查询结果')
    #     time.sleep(3)
    #     self.get_text(ACP.enter_searchassert_loc,doc='输入搜索断言元素')



    # #商品货号元素
    # product_code_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[2]/div/div/input')
    # #商品分类元素
    # commodity_class_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[3]/div/span')
    # #商品品牌元素
    # commodity_brand_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[4]/div/div/div[1]/input')
    # #上架状态元素
    # shelf_status_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[5]/div/div/div[1]')
    # #审核状态元素
    # audit_status_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/form/div[6]/div/div/div[1]')
    # #重置元素
    # reset_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[1]/button[2]')
    # #查询结果元素
    # query_results_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[1]/button[1]')